#include "FractalCreator.h"

#include "Mandelbrot.h"

FractalCreator::FractalCreator(int width, int height) :
	m_width(width),
	m_height(height),
	m_histogram(new int[Mandelbrot::MAX_ITERATIONS]{}),
	m_fractalIterations(new int[m_width*m_height]{}),
	m_bitmap(m_width, m_height),
	m_zoomList(m_width, m_height)
{
	m_zoomList.add(Zoom(m_width / 2, m_height / 2, 4.0 / m_width));
}

void FractalCreator::addZoom(const Zoom & zoom)
{
	m_zoomList.add(zoom);
}

void FractalCreator::run(std::string filename)
{
	calculateIterations();
	calculateTotalIterationsNumber();
	calculateTotalPixelNumberInRange();
	drawFractal();
	writeBitmap(filename);
}

void FractalCreator::addRange(double rangeEnd, const RGB &rgb)
{
	m_ranges.push_back(int(rangeEnd * Mandelbrot::MAX_ITERATIONS));
	m_colors.push_back(rgb);

	if (m_bGotFirstRange)
		m_pixelsNumberInRange.push_back(0);
	m_bGotFirstRange = true;
}

void FractalCreator::calculateIterations()
{
	for (int x = 0; x < m_width; x++)
		for (int y = 0; y < m_height; y++)
		{
			std::pair<double, double> coordinates = m_zoomList.doZoom(x, y);
			int iterations = Mandelbrot::getIterations(coordinates.first, coordinates.second);
			m_fractalIterations[y * m_width + x] = iterations;
			if (iterations != Mandelbrot::MAX_ITERATIONS)
				m_histogram[iterations]++;
		}
}

void FractalCreator::calculateTotalIterationsNumber()
{
	m_totalIterationsNumber = 0;
	for (int i = 0; i < Mandelbrot::MAX_ITERATIONS; i++)
		m_totalIterationsNumber += m_histogram[i];
}

void FractalCreator::calculateTotalPixelNumberInRange()
{
	int rangeIndex = 0;
	for (int i = 0; i < Mandelbrot::MAX_ITERATIONS; i++)
	{
		int pixels = m_histogram[i];
		if (i >= m_ranges[rangeIndex + 1])
			rangeIndex++;
		m_pixelsNumberInRange[rangeIndex] += pixels;
	}

}

void FractalCreator::drawFractal()
{
	for (int x = 0; x < m_width; x++)
		for (int y = 0; y < m_height; y++)
		{
			int iterations = m_fractalIterations[y * m_width + x];

			int range = getRange(iterations);
			int rangeTotal = m_pixelsNumberInRange[range];
			int rangeStart = m_ranges[range];

			RGB &startColor = m_colors[range];
			RGB &endColor = m_colors[range + 1];
			RGB colorDifference = endColor - startColor;

			std::uint8_t red = 0;
			std::uint8_t green = 0;
			std::uint8_t blue = 0;

			if (iterations != Mandelbrot::MAX_ITERATIONS)
			{
				int totalPixels = 0;
				for (int i = rangeStart; i <= iterations; i++)
					totalPixels += m_histogram[i];

				double hue = double(totalPixels) / rangeTotal;
				red = startColor.r + colorDifference.r * hue * hue;
				green = startColor.g + colorDifference.g * hue * hue;
				blue = startColor.b + colorDifference.b * hue * hue;

			}
			m_bitmap.setPixel(x, y, red, green, blue);
		}
}

void FractalCreator::writeBitmap(const std::string & filename)
{
	m_bitmap.write(filename);
}

int FractalCreator::getRange(int iterations) const
{
	int range = 0;
	for (int i = 1; i < m_ranges.size(); i++)
	{
		range = i;
		if (m_ranges[i] > iterations)
			break;
	}
	range--;
	return range;
}
