#include "Bitmap.h"

#include <fstream>

#include "BitmapInfoHeader.h"
#include "BitmapFileHeader.h"

Bitmap::Bitmap(int width, int height) : m_width(width), m_height(height), m_pPixel(new std::uint8_t[width*height * 3]{})
{
    // empty
}

Bitmap::~Bitmap()
{
    // empty
}

int Bitmap::getWidth()
{
    return m_width;
}

int Bitmap::getHeight()
{
    return m_height;
}

bool Bitmap::write(std::string filename)
{
    BitmapFileHeader fileHeader;
    fileHeader.fileSize = sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader) + m_width * m_height * 3;
    fileHeader.dataOffset = sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader);

    BitmapInfoHeader infoHeader;
    infoHeader.width = m_width;
    infoHeader.height = m_height;

    std::ofstream file;
    file.open(filename, std::ios::out | std::ios::binary);
    if (!file)
        return false;

    file.write((char *)&fileHeader, sizeof(fileHeader));
    file.write((char *)&infoHeader, sizeof(infoHeader));
    file.write((char *)m_pPixel.get(), m_width*m_height * 3);

    file.close();
    if (!file)
        return false;

    return true;
}

void Bitmap::setPixel(int x, int y, std::uint8_t red, std::uint8_t green, std::uint8_t blue)
{
    std::uint8_t *pPixel = getPixelPointer(x, y);
    pPixel[0] = blue;
    pPixel[1] = green;
    pPixel[2] = red;
}

std::uint8_t * Bitmap::getPixelPointer(int x, int y)
{
    return m_pPixel.get() + 3 * y * m_width + 3 * x;
}
