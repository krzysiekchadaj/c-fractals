#include <iostream>

#include "FractalCreator.h"
#include "Zoom.h"

#include "RGB.h"

int main()
{
	FractalCreator fractalCreator(800, 600);

	fractalCreator.addRange(0.0, RGB(0, 0, 0));
//	fractalCreator.addRange(0.5, RGB(0, 125, 0));
	fractalCreator.addRange(1.0, RGB(0, 255, 0));

	fractalCreator.addZoom(Zoom(295, 202, 0.1));
	fractalCreator.addZoom(Zoom(312, 304, 0.1));

	fractalCreator.run("fractal.bmp");
	return 0;
}
