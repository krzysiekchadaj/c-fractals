#ifndef FRACTALCREATOR_H
#define FRACTALCREATOR_H

#include <string>
#include <memory>
#include <vector>

#include "Bitmap.h"
#include "RGB.h"
#include "Zoom.h"
#include "ZoomList.h"

class FractalCreator
{
private:
	int m_width;
	int m_height;
	std::unique_ptr<int[]> m_histogram;
	std::unique_ptr<int[]> m_fractalIterations;
	Bitmap m_bitmap;
	ZoomList m_zoomList;
	int m_totalIterationsNumber{ 0 };
	std::vector<int> m_ranges;
	std::vector<RGB> m_colors;
	std::vector<int> m_pixelsNumberInRange;
	bool m_bGotFirstRange{ false };

public:
	FractalCreator(int width, int height);
	void addZoom(const Zoom &zoom);
	void run(std::string filename);
	void addRange(double rangeEnd, const RGB &rgb);

private:
	void calculateIterations();
	void calculateTotalIterationsNumber();
	void calculateTotalPixelNumberInRange();
	void drawFractal();
	void writeBitmap(const std::string &filename);
	int getRange(int iterations) const;
};

#endif
