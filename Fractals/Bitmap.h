#ifndef BITMAP_H
#define BITMAP_H

#include <cstdint>
#include <memory>
#include <string>

class Bitmap
{
private:
    int m_width{ 0 };
    int m_height{ 0 };
    std::unique_ptr<std::uint8_t[]> m_pPixel{ nullptr };

public:
    Bitmap(int width, int height);
    ~Bitmap();

    int getWidth();
    int getHeight();

    bool write(std::string filename);
    void setPixel(int x, int y, std::uint8_t red, std::uint8_t green, std::uint8_t blue);

private:
    std::uint8_t * getPixelPointer(int x, int y);
};

#endif
